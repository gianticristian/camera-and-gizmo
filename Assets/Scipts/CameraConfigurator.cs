using UnityEngine;
using System.Collections;
using UnityEngine.UI;                                       // Required when using UI elements in scripts


public class CameraConfigurator : MonoBehaviour {
    // Los declare del tipo 'GameObject' porque todavia no encuentro bien de que tipo son
    public Text currentSet;
    public Button button;
    public Slider orthographicSize;
    public Text orthographicText;
    public Slider fov;
    public Text fovText;
    public Slider near;
    public Text nearText;
    public Slider far;
    public Text farText;

    void Awake()
    {
        int b = 1;
    }

    void Start () {                                             /// Configura el inicio
        if (Camera.main.orthographic == false) {                // Perspectiva
            currentSet.text = "This is Perspective";            // Texto que indica la configuracion actual
            button.GetComponentInChildren<Text>().text = "Orthographic";    // Cambia el texto de boton                       
            orthographicSize.interactable = false;              // Desactiva el slider de ortografica
            fov.interactable = true;                            // Activa el slider de perspectiva
        }
        else {                                                  // Ortografica
            currentSet.text = "This is Orthographic";
            button.GetComponentInChildren<Text>().text = "Perspective";
            fov.interactable = false;
            orthographicSize.interactable = true;   
        }
        orthographicSize.value = Camera.main.orthographicSize;  // Copia el valor seteado por defecto
        fov.value = Camera.main.fieldOfView;
        near.value = Camera.main.nearClipPlane;
        far.value = Camera.main.farClipPlane;
    }

    // Por alguna razon el boton para activar la camara ortografica funciona para _
    // _ 'true', pero luego no vuelve a 'false'. Por eso no me quedo otra salida _
    // _ que escribir este pequeño codigo y llamarlo desde el evento click del boton
    public void ChangeProjection () {                           /// Ortographic ON/OFF
        if (Camera.main.orthographic == true) {
            Camera.main.orthographic = false;
            currentSet.text = "This is Perspective";
            button.GetComponentInChildren<Text>().text = "Orthographic";
            orthographicSize.interactable = false;
            fov.interactable = true;
        }
        else {
            Camera.main.orthographic = true;
            currentSet.text = "This is Orthographic";
            button.GetComponentInChildren<Text>().text = "Perspective";
            fov.interactable = false;
            orthographicSize.interactable = true;
        }
    }

    void Update () {
        // Actualiza los textos que muestran los valores de los sliders
        fovText.text = fov.value.ToString("n" + 0);             // Muestra solo un numero despues de la coma _
        nearText.text = near.value.ToString("n" + 0);           // _ tengo que ver bien porque es la "n"
        farText.text = far.value.ToString("n" + 0);
        orthographicText.text = orthographicSize.value.ToString("n" + 0);

     
    }

    void OnDrawGizmos () {
        Gizmos.color = Color.yellow;
        Gizmos.matrix = transform.localToWorldMatrix;           // Para que tome la rotacion de la camara
        Gizmos.DrawFrustum(transform.position, Camera.main.fieldOfView, Camera.main.nearClipPlane, Camera.main.farClipPlane, Camera.main.aspect);
    }

    void TestMethod()
    {
	
       int a = 0;
       bool test = false;
       int e = 2;

	float t = 1.1f;



       if (!test) a = 1;
	bool asd = false;        	
       float c = 0.1f;
    }
}
